@echo off
cd "%~dp0"

cksetup run cksetup-fdd-lib.xml -v Debug -l cksetup-fdd-lib.log
cksetup run cksetup-fdd-web.xml -v Debug -l cksetup-fdd-web.log
cksetup run cksetup-scd-lib.xml -v Debug -l cksetup-scd-lib.log
cksetup run cksetup-scd-web.xml -v Debug -l cksetup-scd-web.log
