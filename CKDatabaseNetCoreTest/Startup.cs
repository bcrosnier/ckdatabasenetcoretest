﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Imageflow.Fluent;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace CKDatabaseNetCoreTest
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.Run(async (context) =>
            {
                //await context.Response.WriteAsync("Hello World!");

                // ImageFlow random test
                // This is just a 1x1 px image.
                var imageBytes = Convert.FromBase64String("iVBORw0KGgoAAAANSUhEUgAAAAEAAAABAQMAAAAl21bKAAAAA1BMVEX/TQBcNTh/AAAAAXRSTlPM0jRW/QAAAApJREFUeJxjYgAAAAYAAzY3fKgAAAAASUVORK5CYII=");

                using (var b = new FluentBuildJob())
                {
                    var result = await b.Decode(imageBytes)
                        .FlipHorizontal()
                        .Rotate90()
                        .EncodeToBytes(new GifEncoder())
                        .FinishAsync();

                    context.Response.ContentType = "image/gif";

                    var arraySegment = result.First.TryGetBytes();
                    if (arraySegment.HasValue)
                    {
                        var bytes = arraySegment.Value.ToArray();
                        await context.Response.Body.WriteAsync(bytes, 0, bytes.Length);
                    }



                }
            });


        }
    }
}
