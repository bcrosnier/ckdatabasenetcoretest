@echo off
cd "%~dp0"
if exist "publish" rd /q /s "publish"

REM # FDD WEB
dotnet publish CKDatabaseNetCoreTest/CKDatabaseNetCoreTest_Web.csproj --framework netcoreapp2.0 --output ../publish/FDD_Web
REM # FDD LIB
dotnet publish CKDatabaseNetCoreTest/CKDatabaseNetCoreTest_Lib.csproj --framework netcoreapp2.0 --output ../publish/FDD_Lib

REM # SCD WEB
dotnet publish CKDatabaseNetCoreTest/CKDatabaseNetCoreTest_Web.csproj --framework netcoreapp2.0 --runtime win7-x64 --output ../publish/SCD_Web
REM # SCD LIB
dotnet publish CKDatabaseNetCoreTest/CKDatabaseNetCoreTest_Lib.csproj --framework netcoreapp2.0 --runtime win7-x64 --output ../publish/SCD_Lib
