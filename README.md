# CK-Database test with .NET Core

Tests de déploiement de CKSetup:
- Pour des projets qui target le SDK standard (`Microsoft.NET.Sdk`) et le SDK web (`Microsoft.NET.Sdk.Web`)

## How to reproduce

- Installer un CKSetup dans le `PATH`
- Cloner le repo
- Exécuter `publish-all.bat`
- Exécuter `cksetup-all.bat`

## Description

Ce repo contient :

- Une solution avec:
  - Un projet .netcoreapp2.0 avec deux .csproj - un qui target le SDK web, un qui target le SDK standard
  - Un projet .netstandard2.0 qui est un petit package CK-Database, qui référence UserPassword et c'est tout
- Des utilitaires:
  - `publish-all.bat`
    - Publie le projet netcoreapp2.0 avec le SDK web et standard, en FDD et en SCD, dans 4 dossiers sous `publish/`
  - `cksetup-all.bat`
    - Exécute le CKSetup dans le `PATH` sur les 4 dossiers publiés
- Des `cksetup-*.xml` trimmés qui target les 4 dossiers publiés dans 4 databases différentes

## Notes

- Le projet .netcoreapp2.0 dépend d'une lib native
  - En SCD: `imageflow.dll`
  - En FDD: `runtimes\win7-x64\native\imageflow.dll`
- Le projet .netcoreapp2.0 est en configuration Web par défaut (il utilise par ex. `Microsoft.AspNetCore.All`)
- L'`OutputType` n'est pas précisé, et est une DLL par défaut.
  - Le `publish` SCD Web est le seul qui contient un `.exe`. Normal; c'est le seul cas où le SDK web est utilisé (qui infère un éxécutable), *et* où le framework n'est pas disponible pour éxécuter la DLL.

## Status (at time of writing)

### FDD qui target the SDK Web: `ArgumentException`

```
|  > Info: Launching CKSetup.Runner process.
|  |  > Trace: dotnet CKSetup.Runner.dll merge-deps /silent /logPipe:1112
|  |  |  > Info: Starting CKSetup.Runner - 6.0.0-rc.9 (6.0.0-r09) - SHA1: 034eb77c8b32609785d4e6d4bca206305c7f3a62 - CommitDate: 2018-02-13 16:49:03Z
|  |  |  |  > Info: Merging deps.json files.
|  |  |  |  |  - Info: Merging 'CK.Setupable.Engine.deps.json'.
|  |  |  |  |          Merging 'CK.Setupable.Runtime.deps.json'.
|  |  |  |  |          Merging 'CK.SqlServer.Setup.Engine.deps.json'.
|  |  |  |  |          Merging 'CK.SqlServer.Setup.Runtime.deps.json'.
|  |  |  |  |          Merging 'CK.StObj.Engine.deps.json'.
|  |  |  |  |          Merging 'CK.StObj.Runtime.deps.json'.
|  |  |  |  |          Merging 'CKDatabaseNetCoreTest_Web.deps.json'.
|  |  |  |  |          Saving 'CKSetup.Runner.deps.json.merged'.
|  |  |  |  - Fatal: Can not add property Microsoft.NETCore.DotNetHostPolicy to Newtonsoft.Json.Linq.JObject. Property with the same name already exists on object.
|  |  |  |   ┌──────────────────────────■ Exception: ArgumentException ■──────────────────────────
|  |  |  |   | Message:    Can not add property Microsoft.NETCore.DotNetHostPolicy to Newtonsoft.Json.Linq.JObject. Property with the same name already exists on object.
|  |  |  |   | Stack:         at Newtonsoft.Json.Linq.JObject.ValidateToken(JToken o, JToken existing)
|  |  |  |   |                at Newtonsoft.Json.Linq.JContainer.InsertItem(Int32 index, JToken item, Boolean skipParentCheck)
|  |  |  |   |                at Newtonsoft.Json.Linq.JContainer.AddInternal(Int32 index, Object content, Boolean skipParentCheck)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.AddDependencies(JObject libraryObject, IEnumerable`1 dependencies)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.WritePortableTargetLibrary(RuntimeLibrary runtimeLibrary, CompilationLibrary compilationLibrary)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.WritePortableTarget(IReadOnlyList`1 runtimeLibraries, IReadOnlyList`1 compilationLibraries)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.WriteTargets(DependencyContext context)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.Write(DependencyContext context)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.Write(DependencyContext context, Stream stream)
|  |  |  |   |                at CKSetup.Runner.ActualRunner.MergeDeps(IActivityMonitor m) in C:\CK-World\CK-Database-Projects\CK-Setup\CKSetup.Runner\ActualRunner.cs:line 130
|  |  |  |   |                at CKSetup.Runner.ActualRunner.Run(StringBuilder rawLogText, String[] args, Func`1 monitoredConflicts) in C:\CK-World\CK-Database-Projects\CK-Setup\CKSetup.Runner\ActualRunner.cs:line 29
|  |  |  |   └─────────────────────────────────────────────────────────────────────────
|  |  |  |  - Info: No assembly load conflicts.
|  |  |  |  > Error: Process returned ExitCode 2. CKSetup.Runner.RawLogs.txt file content:
|  |  |  |  |  - Debug: CKSetup.Runner - 6.0.0-rc.9 (6.0.0-r09) - SHA1: 034eb77c8b32609785d4e6d4bca206305c7f3a62 - CommitDate: 2018-02-13 16:49:03Z
```

### SCD qui target the SDK Web: `ArgumentException`

```
|  > Info: Launching CKSetup.Runner process.
|  |  > Trace: dotnet CKSetup.Runner.dll merge-deps /silent /logPipe:1344
|  |  |  > Info: Starting CKSetup.Runner - 6.0.0-rc.9 (6.0.0-r09) - SHA1: 034eb77c8b32609785d4e6d4bca206305c7f3a62 - CommitDate: 2018-02-13 16:49:03Z
|  |  |  |  > Info: Merging deps.json files.
|  |  |  |  |  - Info: Merging 'CK.Setupable.Engine.deps.json'.
|  |  |  |  |          Merging 'CK.Setupable.Runtime.deps.json'.
|  |  |  |  |          Merging 'CK.SqlServer.Setup.Engine.deps.json'.
|  |  |  |  |          Merging 'CK.SqlServer.Setup.Runtime.deps.json'.
|  |  |  |  |          Merging 'CK.StObj.Engine.deps.json'.
|  |  |  |  |          Merging 'CK.StObj.Runtime.deps.json'.
|  |  |  |  |          Merging 'CKDatabaseNetCoreTest_Web.deps.json'.
|  |  |  |  |          Saving 'CKSetup.Runner.deps.json.merged'.
|  |  |  |  - Fatal: Can not add property Microsoft.NETCore.DotNetHostPolicy to Newtonsoft.Json.Linq.JObject. Property with the same name already exists on object.
|  |  |  |   ┌──────────────────────────■ Exception: ArgumentException ■──────────────────────────
|  |  |  |   | Message:    Can not add property Microsoft.NETCore.DotNetHostPolicy to Newtonsoft.Json.Linq.JObject. Property with the same name already exists on object.
|  |  |  |   | Stack:         at Newtonsoft.Json.Linq.JObject.ValidateToken(JToken o, JToken existing)
|  |  |  |   |                at Newtonsoft.Json.Linq.JContainer.InsertItem(Int32 index, JToken item, Boolean skipParentCheck)
|  |  |  |   |                at Newtonsoft.Json.Linq.JContainer.AddInternal(Int32 index, Object content, Boolean skipParentCheck)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.AddDependencies(JObject libraryObject, IEnumerable`1 dependencies)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.WritePortableTargetLibrary(RuntimeLibrary runtimeLibrary, CompilationLibrary compilationLibrary)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.WritePortableTarget(IReadOnlyList`1 runtimeLibraries, IReadOnlyList`1 compilationLibraries)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.WriteTargets(DependencyContext context)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.Write(DependencyContext context)
|  |  |  |   |                at Microsoft.Extensions.DependencyModel.DependencyContextWriter.Write(DependencyContext context, Stream stream)
|  |  |  |   |                at CKSetup.Runner.ActualRunner.MergeDeps(IActivityMonitor m) in C:\CK-World\CK-Database-Projects\CK-Setup\CKSetup.Runner\ActualRunner.cs:line 130
|  |  |  |   |                at CKSetup.Runner.ActualRunner.Run(StringBuilder rawLogText, String[] args, Func`1 monitoredConflicts) in C:\CK-World\CK-Database-Projects\CK-Setup\CKSetup.Runner\ActualRunner.cs:line 29
|  |  |  |   └─────────────────────────────────────────────────────────────────────────
|  |  |  |  - Info: No assembly load conflicts.
|  |  |  |  > Error: Process returned ExitCode 2. CKSetup.Runner.RawLogs.txt file content:
|  |  |  |  |  - Debug: CKSetup.Runner - 6.0.0-rc.9 (6.0.0-r09) - SHA1: 034eb77c8b32609785d4e6d4bca206305c7f3a62 - CommitDate: 2018-02-13 16:49:03Z
```

## FDD qui target le SDK standard: OK

## SCD qui target le SDK standard: OK
